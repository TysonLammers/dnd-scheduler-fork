package com.tlammers.dndScheduler.models

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import java.time.LocalDate

internal class UserGroupTest {

    private lateinit var userGroup : UserGroup

    /**
     * Sets up the user group to a default status
     */
    @BeforeEach
    fun setupUserGroup () {
        val today = LocalDate.now()
        userGroup = UserGroup(1, mutableListOf())
        val userOne = User (
            1,
            "userOne",
            "email_address",
            "firstName",
            "lastName",
            true)
        userOne.setAvailability(today.minusDays(2), AvailabilityStatus.NOT_AVAILABLE)
        userGroup.addUser(userOne)
    }

    @Test
    fun groupIsAvailableOnDay() {
        val today = LocalDate.now()
        assertTrue(userGroup.groupIsAvailableOnDay(today))

        val userTwo = User (
            2,
            "userTwo",
            "email_address",
            "firstName",
            "lastName",
            false)

        userTwo.setAvailability(today, AvailabilityStatus.AVAILABLE)
        userGroup.addUser(userTwo)
        assertTrue(userGroup.groupIsAvailableOnDay(today))
        assertFalse(userGroup.groupIsAvailableOnDay(today.minusDays(1)))
    }
}