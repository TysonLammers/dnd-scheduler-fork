package com.tlammers.dndScheduler.models

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import java.time.LocalDate


internal class UserTest {
    private lateinit var userDefaultAvailable : User
    private lateinit var userDefaultNotAvailable : User

    @BeforeEach
    internal fun setUp () {
        userDefaultAvailable = User (
            1,
            "user_name",
            "email_address",
            "firstName",
            "lastName",
            true)
        userDefaultNotAvailable = User (
            1,
            "user_name",
            "email_address",
            "firstName",
            "lastName",
            false)
    }

    @Test
    fun getId () {
        assertEquals(1, userDefaultAvailable.id)
    }

    @Test
    fun getUsername () {
        assertEquals("user_name", userDefaultAvailable.username)
    }

    @Test
    fun getEmailAddress () {
        assertEquals("email_address", userDefaultAvailable.emailAddress)
    }

    @Test
    fun getFirstName () {
        assertEquals("firstName", userDefaultAvailable.firstName)
    }

    @Test
    fun getLastName () {
        assertEquals("lastName", userDefaultAvailable.lastName)
    }

    @Test
    fun getCalendar () {
        assertEquals(0, userDefaultAvailable.getCalendarSize())
    }

    @Test
    fun testAddToCalendar () {
        val today = LocalDate.now()
        userDefaultAvailable.setAvailability(today, AvailabilityStatus.NOT_AVAILABLE)
        assertFalse(userDefaultAvailable.isAvailableOnDay(today))
        userDefaultNotAvailable.setAvailability(today, AvailabilityStatus.AVAILABLE)
        assertTrue(userDefaultNotAvailable.isAvailableOnDay(today))
    }

    @Test
    fun testEquals () {
        val newUser = userDefaultAvailable.copy()
        assertEquals(newUser, userDefaultAvailable)
    }

    @Test
    fun testDefaultAvailability () {
        val today = LocalDate.now()
        assertFalse(userDefaultNotAvailable.isAvailableOnDay(today))
        assertTrue(userDefaultAvailable.isAvailableOnDay(today))
    }
}