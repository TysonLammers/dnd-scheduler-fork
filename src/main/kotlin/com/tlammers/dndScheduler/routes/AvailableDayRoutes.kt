package com.tlammers.dndScheduler.routes

import com.tlammers.dndScheduler.models.userStorage
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import java.time.LocalDate
import java.time.format.DateTimeParseException

fun Application.availableDayRoutes () {
    routing {
        get("/user/{userId}/calendar/{calendarDate}") {
            val userId = call.parameters["userId"] as? Int ?: return@get call.respondText(
                "Missing or malformed id",
                status = HttpStatusCode.BadRequest
            )
            val calendarDate = try {
                LocalDate.parse(call.parameters["calendarDate"])
            } catch (e: DateTimeParseException) { null } ?: return@get call.respondText(
                "Missing or malformed date",
                status = HttpStatusCode.BadRequest
            )
            val user = userStorage.find { it.id == userId } ?: return@get call.respondText(
                "No user with id $userId",
                status = HttpStatusCode.NotFound
            )
            call.respond(user.isAvailableOnDay(calendarDate))
        }
    }
}
