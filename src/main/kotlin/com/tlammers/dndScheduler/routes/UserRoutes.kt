package com.tlammers.dndScheduler.routes

import com.tlammers.dndScheduler.models.User
import com.tlammers.dndScheduler.models.userStorage
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

/**
 * Defines the REST endpoint for the User entity.
 */
fun Route.userRouting() {
    route("/user") {

        get {
            if(userStorage.isNotEmpty()) {
                call.respond(userStorage)
            } else {
                call.respondText("No users found", status = HttpStatusCode.NotFound)
            }
        }

        get("{id}") {
            val id = call.parameters["id"] ?: return@get call.respondText(
                "Missing or malformed id",
                status = HttpStatusCode.BadRequest
            )
            val user = userStorage.find { it.id == id as? Int } ?: return@get call.respondText(
                "No user with id $id",
                status = HttpStatusCode.NotFound
            )
            call.respond(user)
        }

        post {
            val user = call.receive<User>()
            userStorage.add(user)
            call.respondText("User created successfully", status = HttpStatusCode.Created)
        }

        delete("{id}") {
            val id = call.parameters["id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
            if(userStorage.removeIf { it.id == id as? Int }) {
                call.respondText("User removed successfully", status = HttpStatusCode.Accepted)
            } else {
                call.respondText("Not found", status = HttpStatusCode.NotFound)
            }
        }
    }
}

/**
 * Hook the User routes up to the application.
 */
fun Application.registerUserRoutes() {
    routing {
        userRouting()
    }
}