package com.tlammers.dndScheduler.plugins

import com.tlammers.dndScheduler.routes.availableDayRoutes
import com.tlammers.dndScheduler.routes.registerUserRoutes
import io.ktor.routing.*
import io.ktor.application.*
import io.ktor.http.content.*

fun Application.configureRouting() {

    routing {
        /**
         * Static resources for the frontend
         */
        static("/") {
            resources("static")
            defaultResource("static/index.html")
        }
    }
    registerUserRoutes()
    availableDayRoutes()
}
