package com.tlammers.dndScheduler.models

import kotlinx.serialization.Serializable

@Serializable
enum class AvailabilityStatus {
    AVAILABLE,
    NOT_AVAILABLE,
}