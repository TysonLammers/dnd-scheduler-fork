package com.tlammers.dndScheduler.models

import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import java.time.LocalDate

@Serializable
data class User (
    val id: Int,
    var username: String,
    var emailAddress: String,
    var firstName: String,
    var lastName: String,
    var defaultToAvailable: Boolean,
) {
    private val calendar : MutableMap<@Contextual LocalDate, AvailabilityStatus> = mutableMapOf()

    /**
     * Returns true if the user is available on a day, or defaults to the user's setting
     * if they have not specified availability for the given day.
     */
    fun isAvailableOnDay (date: LocalDate) : Boolean {
        return AvailabilityStatus.AVAILABLE == (calendar[date] ?: return defaultToAvailable)
    }

    /**
     * Sets the availability for a particular date.
     */
    fun setAvailability (date: LocalDate, status: AvailabilityStatus) {
        calendar[date] = status
    }

    fun getCalendarSize () : Int {
        return calendar.size
    }
}

val userStorage = mutableListOf<User>()
