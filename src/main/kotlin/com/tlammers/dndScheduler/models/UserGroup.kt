package com.tlammers.dndScheduler.models

import kotlinx.serialization.Serializable
import java.time.LocalDate

/**
 * An entity that represents a group of users.
 */
@Serializable
data class UserGroup (
    var id: Int,
    var users: MutableList<User> = mutableListOf(),
) {

    /**
     * A function to determine whether all users are available on a particular day.
     */
    fun groupIsAvailableOnDay (day: LocalDate) : Boolean {
        return users.all { user -> user.isAvailableOnDay(day) }
    }

    /**
     * Adds a user to the group.
     */
    fun addUser (user: User) {
        users.add(user)
    }
}
