import React from 'react'
import logo from '../../logo.png'
import './header.css'
import HeaderTitle from './headerTitle.jsx'

function Header () {
    return (
        <header className="App-header">
            <div className="flex-container">
                <img src={logo} className="App-logo" alt="logo" />
                <HeaderTitle />
            </div>
        </header>
    )
}

export default Header