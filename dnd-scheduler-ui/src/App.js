import logo from './logo.png';
import './App.css';
import Header from './components/header/header.jsx'
import AppBody from './components/body/body.jsx'

function App() {
  return (
    <div className="App">
      <Header />
      
        
      <AppBody />
        
    </div>
  );
}

export default App;
